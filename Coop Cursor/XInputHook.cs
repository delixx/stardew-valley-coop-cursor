﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using StardewModdingAPI.Utilities;
using StardewValley;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Coop_Cursor;

/// <summary>
/// SDV switched from XInput to SDL and Nucleus Co-ops wip SDL hook didn't work on SDV.
/// This Class essentially reads the XInput state and maps it to stardews GamePadState
/// </summary>

[StructLayout(LayoutKind.Sequential)]
public struct XInputState
{
    public uint dwPacketNumber; // State packet number
    public XInputGamepad Gamepad; // Gamepad data
}

[StructLayout(LayoutKind.Sequential)]
public struct XInputGamepad
{
    //public ushort wButtons; // Buttons bitmask
    public XInputButton wButtons;
    public byte bLeftTrigger; // Left trigger
    public byte bRightTrigger; // Right trigger
    public short sThumbLX; // Left thumbstick X-axis
    public short sThumbLY; // Left thumbstick Y-axis
    public short sThumbRX; // Right thumbstick X-axis
    public short sThumbRY; // Right thumbstick Y-axis
}

[Flags]
public enum XInputButton : ushort
{
    DPadUp = 0x0001,
    DPadDown = 0x0002,
    DPadLeft = 0x0004,
    DPadRight = 0x0008,
    Start = 0x0010,
    Back = 0x0020,
    LeftThumb = 0x0040,
    RightThumb = 0x0080,
    LeftShoulder = 0x0100,
    RightShoulder = 0x0200,
    A = 0x1000,
    B = 0x2000,
    X = 0x4000,
    Y = 0x8000
}

public class XInputHook
{
    public static PerScreen<XInputState> State = new(() => new());
    private const float ThumbstickDeadzone = 0.25f;
    private const float TriggerDeadzone = 0.1f;

    public static GamePadState GetXInputState(PlayerIndex playerIndex)
    {
        var controllerIndex = (uint)playerIndex;

        var state = new XInputState();
        int result = XInputGetState(controllerIndex, ref state);

        var thumbsticks = MapThumbSticks(state.Gamepad);
        var triggers = MapTriggers(state.Gamepad);
        var buttons = MapButtonsBitmask(state.Gamepad);

        var dpad = new GamePadDPad(
            buttons.HasFlag(Buttons.DPadUp) ? ButtonState.Pressed : ButtonState.Released,
            buttons.HasFlag(Buttons.DPadDown) ? ButtonState.Pressed : ButtonState.Released,
            buttons.HasFlag(Buttons.DPadLeft) ? ButtonState.Pressed : ButtonState.Released,
            buttons.HasFlag(Buttons.DPadRight) ? ButtonState.Pressed : ButtonState.Released);

        var gamepad = new GamePadState(thumbsticks, triggers, new GamePadButtons(buttons), dpad);

        //gamepad.PacketNumber = (int)state.dwPacketNumber;

        return gamepad;
    }

    private static GamePadTriggers MapTriggers(XInputGamepad g)
    {
        //Applies the deadzone and converts from byte to float
        var deadzone = TriggerDeadzone * byte.MaxValue;
        float Convert(byte val) => (val < deadzone && val > -deadzone) ? 0 : val / 255f;

        return new GamePadTriggers(Convert(g.bLeftTrigger), Convert(g.bRightTrigger));
    }


    private static GamePadThumbSticks MapThumbSticks(XInputGamepad g)
    {
        //Applies the deadzone and converts from short to float
        var deadzone = ThumbstickDeadzone * short.MaxValue;
        float Convert(short val) => (val < deadzone && val > -deadzone) ? 0 : val / 32767f;

        return new GamePadThumbSticks(new(Convert(g.sThumbLX), Convert(g.sThumbLY)), new(Convert(g.sThumbRX), Convert(g.sThumbRY)));
    }

    private static Buttons MapButtonsBitmask(XInputGamepad gamepad)
    {
        var b = gamepad.wButtons;

        var ret = new Buttons();    

        if (b.HasFlag(XInputButton.DPadUp))
            ret |= Buttons.DPadUp;
        if (b.HasFlag(XInputButton.DPadDown))
            ret |= Buttons.DPadDown;
        if (b.HasFlag(XInputButton.DPadLeft))
            ret |= Buttons.DPadLeft;
        if (b.HasFlag(XInputButton.DPadRight))
            ret |= Buttons.DPadRight;

        if (b.HasFlag(XInputButton.A))
            ret |= Buttons.A;
        if (b.HasFlag(XInputButton.B))
            ret |= Buttons.B;
        if (b.HasFlag(XInputButton.X))
            ret |= Buttons.X;
        if (b.HasFlag(XInputButton.Y))
            ret |= Buttons.Y;

        if (b.HasFlag(XInputButton.Start))
            ret |= Buttons.Start;
        if (b.HasFlag(XInputButton.Back))
            ret |= Buttons.Back;

        if (b.HasFlag(XInputButton.LeftShoulder))
            ret |= Buttons.LeftShoulder;
        if (b.HasFlag(XInputButton.RightShoulder))
            ret |= Buttons.RightShoulder;

        if (b.HasFlag(XInputButton.LeftThumb))
            ret |= Buttons.LeftStick;
        if (b.HasFlag(XInputButton.RightThumb))
            ret |= Buttons.RightStick;

        var deadzone = ThumbstickDeadzone * short.MaxValue; //-32768 to 32767 (left min, right max, down min, up max)

        //Thumbsticks

        if (gamepad.sThumbLX < -deadzone)
            ret |= Buttons.LeftThumbstickLeft;
        else if (gamepad.sThumbLX > deadzone)
            ret |= Buttons.LeftThumbstickRight;

        if (gamepad.sThumbLY < -deadzone)
            ret |= Buttons.LeftThumbstickDown;
        else if (gamepad.sThumbLY > deadzone)
            ret |= Buttons.LeftThumbstickUp;


        if (gamepad.sThumbRX < -deadzone)
            ret |= Buttons.RightThumbstickLeft;
        else if (gamepad.sThumbRX > deadzone)
            ret |= Buttons.RightThumbstickRight;

        if (gamepad.sThumbRY < -deadzone)
            ret |= Buttons.RightThumbstickDown;
        else if (gamepad.sThumbRY > deadzone)
            ret |= Buttons.RightThumbstickUp;

        //Triggers

        var triggerDeadzone = TriggerDeadzone * byte.MaxValue;

        if (gamepad.bLeftTrigger > triggerDeadzone)
            ret |= Buttons.LeftTrigger;
        if (gamepad.bRightTrigger > triggerDeadzone)
            ret |= Buttons.RightTrigger;

        return ret;
    }

    public static GamePadState GetAsGamePadState()
    {
        var test = GamePad.GetState(Game1.playerOneIndex);
        return GetXInputState(Game1.playerOneIndex);
    }

    [DllImport("xinput1_4.dll", EntryPoint = "XInputGetState")]
    private static extern int XInputGetState(uint dwUserIndex, ref XInputState pState);
}