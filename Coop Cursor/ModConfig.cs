﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coop_Cursor;

class ModConfig
{
    public bool enabled { get; set; } = true;
    public bool keyboardPlayer { get; set; } = true;
}
